# defmodule UseAgent do
#     @moduledoc """
#         Uso do módulo Agente.
#     """

#     @doc """
#         Exemplo uso da função Agente.value/1.
#     """
#     def value(key \\ :cursor) do
#         Agente.value()
#         |> Map.get(key)
#     end

#     @doc """
#         Exemplo uso da função Agente.update/2.
#     """
#     def update(key, value) do
#         Agente.update(key, value)
#         {:ok, "value updated to #{value}"}
#     end

#     @doc """
#         Exemplo uso da função Agente.new/2).
#     """
#     def new(key, value) do
#         Agente.update(value, key)
#         {:ok, "new value added"}
#     end

#     @doc """
#         Exemplo uso da função Agente.update/2.
#     """
#     def change_state do
#         IO.puts "===================="
#         ["def456", "ghi789", "jkl101", "mno121", "pqr131", "stu141", "vwx151", "yza151"]
#         |> Enum.map(&do_change/1)
#     end

#     @doc """
#         Exemplo uso da função Agente.update/2.
#         Função privada.
#     """
#     defp do_change(value) do
#        IO.puts "function: do_change/1"
#        IO.puts "Valor antigo: #{Map.get(Agente.value(), :cursor)}"
#        Agente.update(value)
#        IO.puts "Novo valor: #{Map.get(Agente.value(), :cursor)}"
#        Process.sleep(1000)
#        IO.puts "===================="
#     end
# end

# UseAgent.change_state
