defmodule AgenteTest do
  use ExUnit.Case
  doctest Agente

  describe "Test return value" do
    test "Agente.value/1" do
      expected_value = Agente.value()
      assert expected_value == %{cursor: "cccxxx"} or :normal
    end
  end

  describe "Test update" do
    test "Agente.update/2" do
      expected_value = Agente.update(:cursor, "ccxx")
      assert expected_value == :ok
    end
  end

  describe "Test insert new key-value" do
    test "Agente.new/2" do
      expected_value = Agente.new(:new, "new value")
      assert expected_value == :ok
    end
  end

  describe "Test merge map values" do
    test "Agente.merge/1" do
      expected_value = Agente.merge(%{other: "other value"})
      assert expected_value == :ok
    end
  end

  describe "Test delete key" do
    test "Agente.delete/1" do
      expected_value = Agente.delete(:cursor)
      assert expected_value == :ok
    end
  end

  describe "Test drop keys" do
    test "Agente.drop/1" do
      Agente.new(:foo, "foo")
      Agente.new(:bar, "bar")
      expected_value = Agente.delete([:foo, :bar])
      assert expected_value == :ok
    end
  end
end
